Instalacion de JDK e inicio de Apache-Tomcat
============================================

Despues de habernos encargado del Firewalld ahora si entremos en materia,vamos a instalar y arrancar el servidor Apache-Tomcat. 

Para realizar la instalacion del JDK personalizado descargaremos el paquete compilado de la pagina oficial de oracle, y lo dejaremos en el directorio de instalacion en nuestro caso /opt ::

   [root@srv-apche-tomcat opt]# pwd
   /opt
   [root@srv-apche-tomcat opt]# ls
   jdk-12.0.2_linux-x64_bin.tar.gz

Descomprimimos el jdk para su instalacion ::

   tar -xzvf archivo.tar.gz
   cd jdk-12.0.2

Instalar jdk-12 con alternativas
++++++++++++++++++++++++++++

El comando de alternativas se utiliza para mantener enlaces simbólicos. Este comando crea, elimina, mantiene y muestra información sobre los enlaces simbólicos que componen el sistema de alternativas. Usemos el comando de alternativas para configurar Java en su sistema ::


   alternatives --install /usr/bin/java java /opt/jdk-12.0.2/bin/java 2
   alternatives --config java

La versión de Java recién instalada aparece en el número 2, ingrese 2 y presione Intro. ::

   [root@srv-apche-tomcat jdk-12.0.2]# alternatives --config java

      There are 2 programs which provide 'java'.

         Selection    Command
      -----------------------------------------------
      *+ 1           /opt/jdk1.8.0_201/bin/java
      2           /opt/jdk-12.0.2/bin/java

   Enter to keep the current selection[+], or type selection number: 2

En este punto, JDK-12 se ha instalado correctamente en su sistema. También recomiendo configurar la ruta de comandos javac y jar usando alternativas ::

   alternatives --install /usr/bin/jar jar /opt/jdk1.8.0_201/bin/jar 2
   alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_201/bin/javac 2
   alternatives --set jar /opt/jdk1.8.0_201/bin/jar
   alternatives --set javac /opt/jdk1.8.0_201/bin/javac

Verifiquemos la versión instalada ::

   [root@srv-apche-tomcat jdk-12.0.2]# java -version 
   java version "12.0.2" 2019-07-16
   Java(TM) SE Runtime Environment (build 12.0.2+10)
   Java HotSpot(TM) 64-Bit Server VM (build 12.0.2+10, mixed mode, sharing)

Agregar los siguientes comandos en el archivo /etc/bashrc ::

   export JAVA_HOME=/opt/jdk-12.0.2
   export JRE_HOME=/opt/jdk-12.0.2/jre
   export PATH=$PATH:$JAVA_HOME:$JRE_HOME

Instalacion de Apache Tomcat
++++++++++++++++++++++++

Descargamos el paquete de Apache-tomcat de la `URL Oficial <https://tomcat.apache.org/download-90.cgi>` y descomprimimos en /opt donde ejecutaremos la configuracion del aplicativo ::

   tar -xzvf apache-tomcat-9.0.34.tar.gz

Creamos el usario tomcat para ya que no queremos que el aplicativo suba como root y cambiamos el propietario de la carpeta ::

   adduser tomcat 
   chown -R tomcat. /opt/tomcat

y ahora empezamos a trabajar en la configuracion  del aplicativo en el archivo /opt/apache-tomcat-9.0.34/conf/server.xml con el editor de su preferencia ::

   vim /opt/apache-tomcat-9.0.34/conf/server.xml

estando en este archivo cambiar la seccion del Host (name) ::

   <Host name="localhost"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">

::

   <Host name="192.168.8.104"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">

Procedemos a levantar el tomcat ::

   [root@srv-apche-tomcat ~]# /opt/apache-tomcat-9.0.34/bin/catalina.sh start
   Using CATALINA_BASE:   /opt/apache-tomcat-9.0.34
   Using CATALINA_HOME:   /opt/apache-tomcat-9.0.34
   Using CATALINA_TMPDIR: /opt/apache-tomcat-9.0.34/temp
   Using JRE_HOME:        /usr
   Using CLASSPATH:       /opt/apache-tomcat-9.0.34/bin/bootstrap.jar:/opt/apache-tomcat-9.0.34/bin/tomcat-juli.jar
   Tomcat started.

Verificamos si el proceso se ejecuto satisfactoriamente ::

   [root@srv-apche-tomcat ~]# ps -ef | grep java
   root        1707       1 59 15:47 pts/0    00:00:02 /usr/bin/java -Djava.util.logging.config.file=/opt/apache-tomcat-9.0.34/conf/logging.properties -Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager -Djdk.tls.ephemeralDHKeySize=2048 -Djava.protocol.handler.pkgs=org.apache.catalina.webresources -Dorg.apache.catalina.security.SecurityListener.UMASK=0027 -Dignore.endorsed.dirs= -classpath /opt/apache-tomcat-9.0.34/bin/bootstrap.jar:/opt/apache-tomcat-9.0.34/bin/tomcat-juli.jar -Dcatalina.base=/opt/apache-tomcat-9.0.34 -Dcatalina.home=/opt/apache-tomcat-9.0.34 -Djava.io.tmpdir=/opt/apache-tomcat-9.0.34/temp org.apache.catalina.startup.Bootstrap start
   root        1744    1489  0 15:47 pts/0    00:00:00 grep --color=auto java


Consultamos la URL por el puerto del conector asignado ::

  <Connector port="8080" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />



