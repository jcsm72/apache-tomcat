Configuracion de Systemctl para usuarios Apache-Tomcat
====================================================


Despues de instalar el `Apache-Tomcat <https://gitlab.com/jcsm72/bd-knowledg/-/blob/master/Apache-Tomcat/Arrancar-Apache-Tomcat.rst>`_ creamos un link simbolico hacia tomcat: ::
   
   ln -s /opt/apache-tomcat-9.0.34 /opt/tomcat

Creamos un usuario llamado tomcat sin privilegios.: ::

   adduser tomcat

Cambiamos el propietario de /opt/tomcat al usuario tomcat: ::

   chown -R tomcat. /opt/tomcat

Instalamos el paquete authbind. Una vez que se haya instalado authbind, ejecute lo siguiente según los puertos en los que desee que Tomcat escuche ::

   sudo touch /etc/authbind/byport/80
   sudo chmod 500 /etc/authbind/byport/80
   sudo chown tomcat /etc/authbind/byport/80
   sudo touch /etc/authbind/byport/443
   sudo chmod 500 /etc/authbind/byport/443
   sudo chown tomcat /etc/authbind/byport/443

Creamos la plantilla de servicio en /etc/systemd/system/tomcat.service: ::

   [Unit]
   Description=Tomcat 9.0 - jdk-12.0.2
   After=syslog.target network.target

   [Service]
   Type=forking

   User=tomcat
   Group=tomcat

   Environment="JAVA_HOME=/opt/jdk-12.0.2"
   Environment="JAVA_OPTS=-Djava.security.egd=file:///dev/urandom"

   Environment="CATALINA_BASE=/opt/tomcat"
   Environment="CATALINA_HOME=/opt/tomcat"
   Environment="CATALINA_PID=/opt/tomcattemp/tomcat.pid"
   Environment="CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC"


   ExecStart=/usr/local/bin/authbind --deep /opt/tomcat/bin/startup.sh
   ExecStop=/usr/local/bin/authbind --deep /opt/tomcat/bin/shutdown.sh
   
   RestartSec=10
   Restart=always


   [Install]
   WantedBy=multi-user.target

Recargamos el demonios Systemctl, habilitamos e iniciamos el Tomcat ::

   systemctl daemon-reload
   systemctl enable tomcat.service
   systemctl start tomcat.service

`Documentacion Original <https://blog.webhosting.net/how-to-get-tomcat-running-on-centos-7-2-using-privileged-ports-1024/>`_ 
